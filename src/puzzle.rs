use colored::Colorize;
use enum_iterator::Sequence;
use itertools::Itertools;
use serde::Deserialize;
use std::{collections::HashSet, fmt::Display, fs::File, io::BufReader, path::Path, rc::Rc};

const PUZZLE_WIDTH: usize = 9;
const PUZZLE_HEIGHT: usize = 9;
const EXPECTED_SET: [u8; 9] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

trait PuzzleEntries {
    fn items_present(&self) -> HashSet<u8>;
    fn items_absent(&self, expected: HashSet<u8>) -> HashSet<u8>;
}

impl PuzzleEntries for Vec<Option<u8>> {
    fn items_present(&self) -> HashSet<u8> {
        HashSet::from_iter(self.iter().filter_map(|&e| e))
    }

    /// Items Absent
    ///
    /// Return new list where
    fn items_absent(&self, expected: HashSet<u8>) -> HashSet<u8> {
        let source = self.items_present();
        // println!(" exp: {expected:?}");
        // println!("  me: {source:?}");
        let out = HashSet::from_iter(expected.iter().filter(|&e| !source.contains(e)).cloned());
        // println!(" out: {out:?}");

        #[allow(clippy::let_and_return)]
        out
    }
}

#[derive(Debug, Sequence)]
pub enum PuzzleConstraintGroup {
    Column,
    Row,
    Sector,
}

#[derive(Debug, Deserialize)]
pub struct PuzzleFile {
    pub name: Rc<str>,
    pub rows: Vec<Rc<str>>,
}

#[derive(Clone, Debug)]
pub struct Puzzle {
    data: Vec<Vec<Option<u8>>>,
}

impl Display for Puzzle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut puzzle = String::new();

        for (row_num, row) in self.data.iter().enumerate() {
            if row_num > 0 && 0 == (row_num % 3) {
                puzzle += "----------+-----------+----------\n";
            }

            puzzle += row
                .chunks(3)
                .map(|chunk| {
                    chunk
                        .iter()
                        .map(|value| match value {
                            None => (&0, " _ ".into()),
                            Some(n) => (n, format!(" {n} ")),
                        })
                        .map(|(n, item)| {
                            match n {
                                9 => item.bright_red(),
                                8 => item.bright_purple(),
                                7 => item.magenta(),
                                6 => item.bright_green(),
                                5 => item.green(),
                                4 => item.bright_cyan(),
                                3 => item.cyan(),
                                2 => item.bright_blue(),
                                1 => item.red(),
                                _ => item.normal(),
                            }
                            .to_string()
                        })
                        .collect_vec()
                        .join("")
                })
                .collect_vec()
                .join(" | ")
                .as_str();
            puzzle += "\n";
        }

        write!(f, "{puzzle}")
    }
}

impl Puzzle {
    pub fn from_file<P: AsRef<Path>>(file_name: P) -> anyhow::Result<Self> {
        let file = File::open(file_name)?;
        let reader = BufReader::new(file);
        let puzzle_file: PuzzleFile = serde_yaml::from_reader(reader)?;
        let puzzle = puzzle_file.rows.join("|").replace('|', "");
        let numbers = puzzle
            .chars()
            .map(|num| num as u8)
            .map(|value| match value {
                0x31..=0x39 => Some(value - 0x30),
                _ => None,
            })
            .collect_vec();
        let data = numbers
            .chunks(PUZZLE_WIDTH)
            .map(|row| row.iter().cloned().collect_vec())
            .collect_vec();

        Ok(Puzzle { data })
    }

    pub fn get_value_at(&self, x: usize, y: usize) -> Option<u8> {
        self.data[y][x]
    }

    pub fn set_value_at(&mut self, x: usize, y: usize, value: Option<u8>) -> Option<u8> {
        self.data[y][x] = value;

        value
    }

    pub fn possible_values_for(&self, x: usize, y: usize) -> HashSet<u8> {
        let all: HashSet<u8> = HashSet::from_iter(EXPECTED_SET);

        // println!(" all: {:?}", all);
        let free = self.get_column(x).items_absent(all);
        // println!("-------------------");
        // println!("free: {free:?}");
        let free = self.get_row(y).items_absent(free);
        // println!("-------------------");
        // println!("free: {free:?}");
        let free = self.get_sector(x, y).items_absent(free);
        // println!("-------------------");
        // println!("free: {free:?}");

        #[allow(clippy::let_and_return)]
        free
    }

    pub fn get_empty_coordinates(&self) -> Vec<(usize, usize)> {
        (0..PUZZLE_HEIGHT) // for y
            .map(|y| {
                (0..PUZZLE_WIDTH) // for x
                    .filter_map(|x| {
                        // loop
                        if self.get_value_at(x, y).is_none() {
                            Some((x, y))
                        } else {
                            None
                        }
                    })
                    .collect_vec() // Internal vector of values
            })
            .concat() // Join internal vectors into single vector
    }

    pub fn get_by_cg(&self, group: PuzzleConstraintGroup, x: usize, y: usize) -> Vec<Option<u8>> {
        match group {
            PuzzleConstraintGroup::Column => self.get_column(x),
            PuzzleConstraintGroup::Row => self.get_row(y),
            PuzzleConstraintGroup::Sector => self.get_sector(x, y),
        }
    }

    /// Get vertical column of values
    pub fn get_column(&self, x: usize) -> Vec<Option<u8>> {
        self.data.iter().map(|row| row[x]).collect_vec()
    }

    /// Get horizontal row of values
    pub fn get_row(&self, y: usize) -> Vec<Option<u8>> {
        self.data[y].clone()
    }

    /// Get all values from the sector containing coordinate
    pub fn get_sector(&self, x: usize, y: usize) -> Vec<Option<u8>> {
        // Project to sector coordinates
        let u = (x / 3) * 3;
        let v = (y / 3) * 3;

        // Reproject to index coordinates
        (v..v + 3) // for y
            .map(|y| {
                (u..u + 3) // for x
                    .map(|x| {
                        // loop
                        let value = self.get_value_at(x, y);
                        // println!("  -> ({x}, {y}) {value:?}");
                        #[allow(clippy::let_and_return)]
                        value
                    })
                    .collect_vec() // Internal vector of values
            })
            .concat() // Join internal vectors into single vector
    }
}

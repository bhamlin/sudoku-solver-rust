#![allow(unused)]

pub mod cli_args;
pub mod puzzle;

use crate::{cli_args::Opts, puzzle::Puzzle};
use clap::Parser;
use itertools::Itertools;
use log::Level;

fn main() -> anyhow::Result<()> {
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or(Level::Debug.as_str()),
    )
    .init();

    let opts = Opts::parse();

    let mut puzzle = Puzzle::from_file(opts.filename)?;
    println!("{puzzle}");
    println!();

    // println!("{:?}", puzzle.possible_values_for(5, 0));

    // println!(
    //     "{:?}",
    //     puzzle
    //         .get_sector(0, 0)
    //         .iter()
    //         .filter_map(|&e| Some(e?))
    //         .collect_vec()
    // );
    // (0..9).for_each(|y| {
    //     (0..9).for_each(|x| {
    //         println!(
    //             "({x}, {y}) {:?}",
    //             puzzle
    //                 .get_sector(x, y)
    //                 .iter()
    //                 .filter_map(|&e| e)
    //                 .collect_vec()
    //         )
    //     })
    // });

    #[allow(clippy::never_loop)]
    loop {
        let empties = puzzle.get_empty_coordinates();
        let mut uno_mas = false;
        if empties.is_empty() {
            break;
        }
        // println!("{}: {empties:?}", empties.len());

        for (x, y) in empties {
            let missing = puzzle.possible_values_for(x, y);
            if missing.len() == 1 {
                puzzle.set_value_at(x, y, missing.iter().next().cloned());
                uno_mas = true;
            }
        }

        if !uno_mas {
            break;
        }
    }

    let p2 = puzzle.clone();
    println!("{p2}");

    Ok(())
}

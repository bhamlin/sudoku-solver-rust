#pragma once
#ifndef PUZZLE_H
#define PUZZLE_H 1

u8 puzzle_empty_index_list(PUZZLE_BUF index_list, PUZZLE_BUF puzzle);
u8 puzzle_value_at(PUZZLE_BUF puzzle, u8 x, u8 y);
void puzzle_column(PUZZLE_BUF output, PUZZLE_BUF puzzle, u8 x, u8 y);
void puzzle_row(PUZZLE_BUF output, PUZZLE_BUF puzzle, u8 x, u8 y);
void puzzle_sector(PUZZLE_BUF output, PUZZLE_BUF puzzle, u8 x, u8 y);

#endif

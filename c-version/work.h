#pragma once
#ifndef WORK_H
#define WORK_H 1

#define WORK_BUFFER_SIZE PS

void work_clear(WORK_BUF buffer);
void work_set(WORK_BUF buffer, u8 index);
void work_set_remove_(WORK_BUF buffer, WORK_BUF input);
u8 work_count_filled(WORK_BUF buffer);
u8 work_count_empty(WORK_BUF buffer);
u8 work_get_first(WORK_BUF buffer);

#endif

#pragma once
#ifndef SOODOOKOO_H
#define SOODOOKOO_H 1

#define BUFFER_SIZE 512
#define PS 9

typedef unsigned char u8;
typedef unsigned short u16;

typedef u8 *PUZZLE_BUF;
typedef u8 *WORK_BUF;

#endif

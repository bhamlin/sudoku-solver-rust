#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./soodookoo.h"
#include "./puzzle.h"
#include "./work.h"

const u8 PUZZLE[] = {
    0, 0, 0, /*|*/ 2, 6, 0, /*|*/ 7, 0, 1,
    6, 8, 0, /*|*/ 0, 7, 0, /*|*/ 0, 9, 0,
    1, 9, 0, /*|*/ 0, 0, 4, /*|*/ 5, 0, 0,
    /*---------+--------------+---------*/
    8, 2, 0, /*|*/ 1, 0, 0, /*|*/ 0, 4, 0,
    0, 0, 4, /*|*/ 6, 0, 2, /*|*/ 9, 0, 0,
    0, 5, 0, /*|*/ 0, 0, 3, /*|*/ 0, 2, 8,
    /*---------+--------------+---------*/
    0, 0, 9, /*|*/ 3, 0, 0, /*|*/ 0, 7, 4,
    0, 4, 0, /*|*/ 0, 5, 0, /*|*/ 0, 3, 6,
    7, 0, 3, /*|*/ 0, 1, 8, /*|*/ 0, 0, 0
    //
};

const u8 FULL_SET[] = {
    1, 2, 3, 4, 5, 6, 7, 8, 9
    //
};

char u8_to_char(u8 value)
{
    if (0 == value)
    {
        return ' ';
    }
    else
    {
        return '0' + (char)value;
    }
}

char *spr_set(char *output, u8 *arr)
{
    sprintf(output, "[%d, %d, %d, %d, %d, %d, %d, %d, %d]",
            arr[0], arr[1], arr[2], arr[3], arr[4],
            arr[5], arr[6], arr[7], arr[8]);

    return output;
}

u16 pr_set(u8 *arr)
{
    return printf("[%2d, %2d, %2d, %2d, %2d, %2d, %2d, %2d, %2d]\n",
                  arr[0], arr[1], arr[2], arr[3], arr[4],
                  arr[5], arr[6], arr[7], arr[8]);
}

void fill_buffer(u8 *buffer, const u8 *source, u16 qty)
{
    memcpy(buffer, source, qty);
}

void empty_buffer(void *buffer, u16 qty)
{
    memset(buffer, 0x00, qty);
}

/* Expects output to already be cleared */
u16 render_puzzle_to_print(PUZZLE_BUF puzzle, char *output)
{
    u16 cursor = 0;

    u8 *row = (u8 *)malloc(PS);
    for (u8 v = 0; v < PS; v++)
    {
        if ((0 != v) && (0 == (v % 3)))
        {
            cursor += sprintf(output + cursor, "---------+---------+---------\n");
        }

        u8 y = v * PS;
        memcpy(row, puzzle + y, PS);
        cursor += sprintf(output + cursor, " %c  %c  %c | %c  %c  %c | %c  %c  %c \n", //
                          u8_to_char(row[0]), u8_to_char(row[1]), u8_to_char(row[2]),  //
                          u8_to_char(row[3]), u8_to_char(row[4]), u8_to_char(row[5]),  //
                          u8_to_char(row[6]), u8_to_char(row[7]), u8_to_char(row[8]));
    }

    free(row);
    return cursor;
}

int main()
{
    u16 chars;
    u8 p_len = PS * PS;
    PUZZLE_BUF puzzle = (PUZZLE_BUF)malloc(p_len);
    fill_buffer(puzzle, PUZZLE, p_len);

    char *display_buffer = (char *)malloc(BUFFER_SIZE);
    empty_buffer(display_buffer, BUFFER_SIZE);

    chars = render_puzzle_to_print(puzzle, display_buffer);
    printf(display_buffer);
    empty_buffer(display_buffer, chars);
    printf("\n");

    u8 empty_count, uno_mas = 0;
    u8 j, n, u, v;
    PUZZLE_BUF il = (PUZZLE_BUF)malloc(p_len);
    WORK_BUF scratch_buf = (WORK_BUF)malloc(WORK_BUFFER_SIZE);
    WORK_BUF work_buf = (WORK_BUF)malloc(WORK_BUFFER_SIZE);
    do
    {
        empty_count = puzzle_empty_index_list(il, puzzle);
        // printf("Empties: %d\n", empty_count);
        if (empty_count > 0)
        {
            for (u8 i = 0; i < empty_count; i++)
            {
                j = il[i];

                u = j % PS;
                v = j / PS;

                fill_buffer(work_buf, FULL_SET, PS);
                puzzle_row(scratch_buf, puzzle, u, v);
                work_set_remove_(work_buf, scratch_buf);
                puzzle_column(scratch_buf, puzzle, u, v);
                work_set_remove_(work_buf, scratch_buf);
                puzzle_sector(scratch_buf, puzzle, u, v);
                work_set_remove_(work_buf, scratch_buf);
                n = work_count_filled(work_buf);

                if (n > 1)
                {
                    continue;
                }
                else
                {
                    n = work_get_first(work_buf);
                    // printf("(%d, %d) %d\n", u, v, n);
                    puzzle[j] = n;
                    uno_mas += 1;
                }
            }
        }
        else
        {
            break;
        }
    } while (uno_mas);

    chars = render_puzzle_to_print(puzzle, display_buffer);
    printf(display_buffer);

    free(work_buf);
    free(display_buffer);
    free(puzzle);
    return 0;
}

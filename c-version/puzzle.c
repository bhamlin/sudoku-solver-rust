#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./soodookoo.h"
#include "./puzzle.h"
#include "./work.h"

// puzzle_empty_index_list
// ---
// Examine `puzzle`, fill in `index_list` with indicies containing a
// 0 value, and count how many are placed into index_list.
//
// Returns count of entries modified in `index_list`.
u8 puzzle_empty_index_list(PUZZLE_BUF index_list, PUZZLE_BUF puzzle)
{
    u8 count = 0;

    for (u8 i = 0, n = PS * PS; i < n; i++)
    {
        if (0 == puzzle[i])
        {
            index_list[count] = i;
            count += 1;
        }
    }

    return count;
}

u8 puzzle_value_at(PUZZLE_BUF puzzle, u8 x, u8 y)
{
    if ((x >= PS) || (y >= PS))
    {
        return -1;
    }

    return puzzle[(y * PS) + x];
}

void puzzle_column(WORK_BUF output, PUZZLE_BUF puzzle, u8 x, u8 y)
{
    u8 v;
    for (u8 i = 0; i < PS; i++)
    {
        v = i * PS;
        output[i] = puzzle[v + x];
    }
}

void puzzle_row(WORK_BUF output, PUZZLE_BUF puzzle, u8 x, u8 y)
{
    for (u8 i = 0, u = y * PS; i < PS; i++)
    {
        output[i] = puzzle[u + i];
    }
}

void puzzle_sector(WORK_BUF output, PUZZLE_BUF puzzle, u8 x, u8 y)
{
    u8 us = (x / 3) * 3, um = us + 3;
    u8 vs = (y / 3) * 3, vm = vs + 3;
    u8 i = 0;

    for (u8 v = vs; v < vm; v++)
    {
        for (u8 u = us; u < um; u++)
        {
            output[i] = puzzle_value_at(puzzle, u, v);
            i += 1;
        }
    }
}

#include <string.h>

#include "./soodookoo.h"
#include "./work.h"

void work_clear(WORK_BUF buffer)
{
    memset(buffer, 0x00, WORK_BUFFER_SIZE);
}

void work_set(WORK_BUF buffer, u8 index)
{
    if (index < WORK_BUFFER_SIZE)
    {
        buffer[index] = index;
    }
}

void work_set_remove_(WORK_BUF buffer, WORK_BUF input)
{
    u8 n;
    for (u8 i = 0; i < PS; i++)
    {
        n = input[i];
        if (0 != n)
        {
            buffer[n - 1] = 0;
        }
    }
}

u8 work_count_filled(WORK_BUF buffer)
{
    return PS - work_count_empty(buffer);
}

u8 work_count_empty(WORK_BUF buffer)
{
    u8 count = 0;

    for (u8 i = 0; i < WORK_BUFFER_SIZE; i++)
    {
        if (0 == buffer[i])
        {
            count += 1;
        }
    }

    return count;
}

u8 work_get_first(WORK_BUF buffer)
{
    for (u8 i = 0; i < PS; i++)
    {
        if (0 != buffer[i])
        {
            return buffer[i];
        }
    }
    return 0;
}